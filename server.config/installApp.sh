sudo apt update
sudo apt upgrade -y
sudo apt autoremove
sudo apt install curl wget apt-transport-https dirmngr git
DIRCONFGITHUB=~/dotfilles
DIRCONFLOCAL=~/.Dotfiles
if [ -d "$DIRCONFGITHUB" ]; then
	echo "$DIRCONFGITHUB exists."
	cd ~/dotfilles
	git pull
	cd ~

else 
	echo "$DIRCONFGITHUB does not exist."
	git clone https://gitlab.com/bit-agroplus-dz/dotfilles.git
fi


if [ -d "$DIRCONFLOCAL" ]; then
	echo "$DIRCONFLOCAL exists."
	rm -rf ~/.Dotfiles/*
else 
	echo "$DIRCONFLOCAL does not exist."
	mkdir ~/.Dotfiles
fi
cp -r ~/dotfilles/* .Dotfiles/
sudo mv  /etc/apt/sources.list /etc/apt/sources.list.old
sudo cp ~/.Dotfiles/sources.list /etc/apt/sources.list
sudo apt update
sudo apt upgrade -y
# terminator
sudo apt -y install terminator
sudo update-alternatives --config x-terminal-emulator
# ohmyzsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sudo apt install zsh zplug
curl -L git.io/antigen > ~/.Dotfiles/antigen.zsh
# node js
sudo apt-get install curl
sudo curl -o /usr/local/bin/n https://raw.githubusercontent.com/visionmedia/n/master/bin/n
sudo chmod +x /usr/local/bin/n
sudo n stable
# neovim
sudo apt -y install neovim

mkdir ~/.config/nvim/

ln -s ~/.Dotfiles/init.vim ~/.config/nvim/init.vim

npm install -g npm@latest

mkdir -p ~/.local/share/fonts

cd ~/.local/share/fonts && curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf

dpkg-reconfigure fontconfig-config

curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

dpkg-reconfigure fontconfig-config

sudo apt remove --purge vim-ultisnips vim-snippets

sudo apt autoremove
sudo rm -rf /var/lib/vim/addons/pythonx/UltiSnips/snippet
npm i -g eslint eslint-plugin-vue
sudo apt install exuberant-ctags
pip3 install ueberzug

# Brave browser
sudo apt install apt-transport-https curl
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install brave-browser

# gnupg
sudo apt -y install gnupg2

# SSH
sudo apt install openssh-server openssh-client

# ClamAV
sudo apt install clamav

# clamd.remote.conf 

#nmap
sudo apt install nmap

# no-ip
sudo -s
cd /usr/local/src/
wget http://www.no-ip.com/client/linux/noip-duc-linux.tar.gz
tar xf noip-duc-linux.tar.gz
cd noip-2.1.9/
make install
exit
sudo cp noip /etc/init.d/noip
sudo chmod 755 /etc/init.d/noip


# Docker
# sudo -s
# exit
#
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo chmod a+r /etc/apt/keyrings/docker.gpg
apt-cache madison docker-ce
sudo docker run hello-world
## Container


# couchdb
