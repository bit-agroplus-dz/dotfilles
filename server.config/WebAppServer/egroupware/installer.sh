echo 'deb http://download.opensuse.org/repositories/server:/eGroupWare/Debian_11/ /' | sudo tee /etc/apt/sources.list.d/server:eGroupWare.list
sudo apt install gnupg # required, but not installed by apt-key add in Debian 10/11
wget -nv https://download.opensuse.org/repositories/server:eGroupWare/Debian_11/Release.key -O - | sudo apt-key add - | sudo tee /etc/apt/trusted.gpg.d/server:eGroupWare.asc
sudo apt update
