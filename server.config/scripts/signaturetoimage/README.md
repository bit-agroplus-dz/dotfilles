Signature to Image
===

Signature to Image: A supplemental script for Signature Pad that generates
an image of the signature’s JSON output server-side using PHP.

Signature Pad: <http://thomasjbradley.ca/lab/signature-pad>

Copyright MMXI, Thomas J Bradley <hey@thomasjbradley.ca>

Versioned using Semantic Versioning <http://semver.org/>


Complete Documentation
---
<http://thomasjbradley.ca/lab/signature-to-image>


Source Code
---
<http://github.com/thomasjbradley/signature-to-image>


Version History
---
**1.0.1 (Apr. 9, 2011)**

- Added `stripslashes()` to the string JSON for many servers that have magic quotes on.

**1.0.0 (Aug. 29, 2010)**

- Initial Release

