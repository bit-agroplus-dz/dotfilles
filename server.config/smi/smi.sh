#!/bin/bash
mkdir -p ~/.docker/www ~/.docker/mysql ~/config/virtualhosts
chmod 777 ~/.docker/www ~/.docker/mysql
cd ~/.docker/www/ 
mkdir smi
cd smi
wget http://smi.no-ip.org/down/smi_1.0.6.zip
unzip  smi_1.0.6.zip -d .
mkdir telechgt && mkdir telechgt/fictec && mkdir svg && mkdir logos
cd ..
sudo chmod -R 777 smi

<<comment
En console ou en utilisant l'interface graphique phpmyadmin, créer une base de donnée 'smi'. Créer un utilisateur nommé wwwrun et lui donner les droit suivants: 'SELECT', 'INSERT', 'UPDATE', DELETE', 'FILE', 'CREATE ', 'ALTER', 'INDEX', et 'DROP' sur la base smi que vous venez de mettre en place. (voir doc de MySql)

Ouvrir en édition le fichier smi/inc/prm.inc.php, éditer les données dans les champs , ne pas oublier que l'utilisateur est wwwrun, il faut y mettre un mot de passe, laisser les répertoires de téléchargement et sauvegarde par défaut puisque nous les avons créés tels quels. Puis sauver
comment
# sudo nano inc/prm.inc.php
