#!/bin/bash

## lamp:stable
mkdir -p ~/docker/lamp/html
cd ~/docker/lamp
touch php.Dockerfile
echo "
FROM php:7.4.3-apache
RUN docker-php-ext-install mysqli pdo pdo_mysql
" >> php.Dockerfile
cd -
cp index.php ~/docker/lamp/html/
cd ~/docker/lamp
